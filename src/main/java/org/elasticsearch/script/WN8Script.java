/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2017 team-unpro
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */


package org.elasticsearch.script;

import org.elasticsearch.index.fielddata.ScriptDocValues;
import org.elasticsearch.script.AbstractDoubleSearchScript;

import java.util.Map;

public class WN8Script extends AbstractDoubleSearchScript {
    public static final String TANK_ID_FIELD = "tank_id";
    private Map<String, Map<String, Number>> expectedTankValuesMap;

    WN8Script(Map<String, Map<String, Number>> expectedTankValues) {
        expectedTankValuesMap = expectedTankValues;
    }

    @Override
    public double runAsDouble() {
        if (!doc().containsKey(TANK_ID_FIELD)) {
            throw new IllegalArgumentException("Document does not contain field [" + TANK_ID_FIELD + "].");
        }
        Long tankId = ((ScriptDocValues.Longs) doc().get(TANK_ID_FIELD)).getValue();
        Map<String, Number> expectedStats = expectedTankValuesMap.get(tankId.toString());

        if (expectedStats != null) {
            return wn8(getLongValue("damage"), getLongValue("spot"), getLongValue("frags"), getLongValue("defense"),
                    getLongValue("wins"), getLongValue("battles"), expectedStats.get("damage").doubleValue(),
                    expectedStats.get("spot").doubleValue(), expectedStats.get("frags").doubleValue(),
                    expectedStats.get("defense").doubleValue(), expectedStats.get("wins").doubleValue());
        } else {
            return Double.NaN;
        }
    }

    private Long getLongValue(String field) {
        return ((ScriptDocValues.Longs) doc().get(field)).getValue();
    }

    private static double wn8(long damage, long spot, long frags, long defense, long wins, long battles, double expectedDamage,
                              double expectedSpot, double expectedFrags, double expectedDefense, double expectedWins) {
        double rDAMAGE = (damage / (double) battles) / expectedDamage;
        double rSPOT = (spot / (double) battles) / expectedSpot;
        double rFRAG = (frags / (double) battles) / expectedFrags;
        double rDEF = (defense / (double) battles) / expectedDefense;
        double rWIN = (wins / (double) battles) * 100 / expectedWins;

        double rWINc = Math.max(0, (rWIN - 0.71) / (1 - 0.71));
        double rDAMAGEc = Math.max(0, (rDAMAGE - 0.22) / (1 - 0.22));
        double rFRAGc = Math.max(0, Math.min(rDAMAGEc + 0.2, (rFRAG   - 0.12) / (1 - 0.12)));
        double rSPOTc = Math.max(0, Math.min(rDAMAGEc + 0.1, (rSPOT   - 0.38) / (1 - 0.38)));
        double rDEFc = Math.max(0, Math.min(rDAMAGEc + 0.1, (rDEF    - 0.10) / (1 - 0.10)));
        return 980 * rDAMAGEc +
                210 * rDAMAGEc * rFRAGc +
                155 * rFRAGc * rSPOTc +
                75 * rDEFc * rFRAGc +
                145 * Math.min(1.8, rWINc);
    }
}
