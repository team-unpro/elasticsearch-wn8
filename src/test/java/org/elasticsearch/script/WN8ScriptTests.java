/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2017 team-unpro
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */


package org.elasticsearch.script;

import org.apache.lucene.index.SortedNumericDocValues;
import org.elasticsearch.index.fielddata.ScriptDocValues;
import org.elasticsearch.search.lookup.LeafDocLookup;
import org.elasticsearch.search.lookup.LeafSearchLookup;
import org.elasticsearch.test.ESTestCase;

import java.util.Collections;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class WN8ScriptTests extends ESTestCase {
    public void testNoExpectedStatsReturnsNaN() throws Exception {
        LeafDocLookup ldl = mock(LeafDocLookup.class);
        when(ldl.containsKey("tank_id")).thenReturn(true);
        when(ldl.get("tank_id")).thenReturn(new ScriptDocValues.Longs(new SortedNumericDocValues() {
            @Override
            public void setDocument(int doc) {

            }

            @Override
            public long valueAt(int index) {
                return 1;
            }

            @Override
            public int count() {
                return 1;
            }
        }));
        LeafSearchLookup lsl = mock(LeafSearchLookup.class);
        when(lsl.doc()).thenReturn(ldl);
        WN8Script s = new WN8Script(Collections.emptyMap());
        s.setLookup(lsl);
        s.setDocument(1);
        assertEquals(Double.NaN, s.runAsDouble(), 1.0);
    }

}